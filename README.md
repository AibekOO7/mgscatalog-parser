# Парсер для https://mgscatalog.by
Извлекает данные с сайта и сохраняет их в бвзу данных MongoDB.


## Использованные технологии
- [Java 21]
- [Spring Boot]
- [Gradle](https://docs.gradle.org)
- [Spring Shell](https://spring.io/projects/spring-shell)
- [Jsoup](https://jsoup.org/)
- [MongoDB]
- [Apache HttpClient]


## Сборка и выполнение
- сборка: `./gradlew clean build`
- выполнение: `java --enable-preview -jar ./build/libs/mgscatalogparser-1.0.0.jar`
- + необходимые параметры передаются с помощью 
- + - файла `application.properties`
- + - или переменных окружения `export MONGODB_HOST=localhost && export MONGODB_PORT=27017 && export MONGODB_COLLECTION=mgscatalog_collection && export MONGODB_USERNAME=admin && export MONGODB_PASSWORD=123 && export HTTP_CLIENT_MAX_TOTAL_CONNECTIONS=25 && export HTTP_CLIENT_MAX_CONNECTIONS_PER_HOST=25`
- контейнеризация
- + создание образа: `docker build -t mgscatalogparser:1.0.0 .`
- + запуск контейнера(если mongodb запущен в отдельном контейнере, то необходимо добавить свой `--network`): `docker run --rm -ti  -e MONGODB_HOST=172.18.0.2 -e MONGODB_PORT=27017 -e MONGODB_COLLECTION=mgscatalog_collection -e MONGODB_USERNAME=admin -e MONGODB_PASSWORD=123 -e HTTP_CLIENT_MAX_TOTAL_CONNECTIONS=25 -e HTTP_CLIENT_MAX_CONNECTIONS_PER_HOST=25 mgscatalogparser:1.0.0`


## Основные команды
- `help`: Отобразить справку о доступных командах.
- `quit`: Выход из shell.
- `clear-db`: Удалить всех записи из бд.
- `info`: Получить информацию о количестве документов и количестве страниц на сайте.
- `chunk-pages`: Разбить страницы на части.
- `parse`: Извлечь данные и сохранить их в бд.

Детали команды можно посмотреть с помощью `-h`(Пример: parse -h).


## Пимер
- парсинг только на локальной машине 
- - обязательно перед парсингом очистить бд: `clear-db`
- - узнать кол-во страниц: `info`
- - начать парсинг: parse --startPage 1 --endPage 2885 --nThreadsOfListProcess 3 --nThreadsOfDetailProcess 15
- - - --startPage будет 1, так как начинается с первой страницы
- - - --endPage берется из `info`
- - - --nThreadsOfListProcess 3 - это кол-во потоков для прасинга страниц, содержащие сведения о списке каталогов
- - - --nThreadsOfDetailProcess 15 - это это кол-во потоков для прасинга и сохранения страниц, содержащие детальную информацию о каталоге
- - - nThreadsOfListProcess=3 и nThreadsOfDetailProcess=15, вроде оптимальные значения, чтобы обойти ограничения на количество одновременных запросов к сайту с одного ip
- парсинг на нескольких машинах (Пример: пусть будет 3 машины)
- - обязательно перед парсингом очистить бд: `clear-db`
- - узнать кол-во страниц: `info` (Пример: пусть вернет 34 страниц)
- - поделить страницы на независимые части для 3 машин: `chunk-pages --start 1 --end 34 --parts 3`
- - - результат: Часть 1: [1, 11]  Часть 2: [12, 22]  Часть 3: [23, 34]
- - выполнить команду:
- - - на первой машине: `parse --startPage 1 --endPage 11 --nThreadsOfListProcess 3 --nThreadsOfDetailProcess 15`
- - - на второй машине: `parse --startPage 12 --endPage 22 --nThreadsOfListProcess 3 --nThreadsOfDetailProcess 15`
- - - на третьей машине: `parse --startPage 23 --endPage 34 --nThreadsOfListProcess 3 --nThreadsOfDetailProcess 15`




