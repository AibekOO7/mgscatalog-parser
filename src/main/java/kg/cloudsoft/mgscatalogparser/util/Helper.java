package kg.cloudsoft.mgscatalogparser.util;

import lombok.Getter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public record Helper() {

    public static boolean isNumeric(String s) {
        if (s.isBlank()) {
            return false;
        }

        Pattern numericPattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        return numericPattern.matcher(s).matches();
    }

    public static LocalDateTime parseDateTime(String dateString, DateTimeFormat format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format.getFormat());
            return LocalDateTime.parse(dateString, formatter);
        } catch (Exception e) {
            throw new RuntimeException(STR."Ошибка parseDateTime '\{dateString}'", e);
        }
    }

    public static int[][] chunkPages(int start, int end, int parts) {
        if (start <= 0 || end <= 0) {
            throw new IllegalArgumentException("start и end не могут быть меньше 1.");
        }

        if (start >= end) {
            throw new IllegalArgumentException("start должен быть меньше end.");
        }

        if (parts < 2) {
            throw new IllegalArgumentException("parts должен быть больше 1.");
        }

        int total = end - start + 1;
        int remainder = total % parts;
        int pagesPerPart = (total - remainder) / parts;

        int[][] arr = new int[parts][2];
        for (int j = 0; j < parts; j++) {
            int i = j + 1;
            int startI = start + (i - 1) * pagesPerPart;
            int endI = start + i * pagesPerPart - 1;
            arr[j][0] = startI;
            arr[j][1] = endI;

            if (j == parts - 1) {
                arr[j][1] += remainder;
            }
        }

        return arr;
    }

    @Getter
    public enum DateTimeFormat {
        yyyy_MM_dd__HH_mm_ss("yyyy-MM-dd HH:mm:ss");

        private final String format;

        DateTimeFormat(String format) {
            this.format = format;
        }
    }
}
