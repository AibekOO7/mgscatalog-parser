package kg.cloudsoft.mgscatalogparser.mgscatalog;

import com.mongodb.client.MongoCollection;
import kg.cloudsoft.mgscatalogparser.config.HttpClientManager;
import kg.cloudsoft.mgscatalogparser.config.MongoDbManager;
import kg.cloudsoft.mgscatalogparser.util.Helper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

import static kg.cloudsoft.mgscatalogparser.mgscatalog.Constants.*;

@Slf4j
@RequiredArgsConstructor
@Component
final class MgscatalogManager implements AutoCloseable {

    private final MongoDbManager mongoDbManager;
    private final HttpClientManager httpClientManager;

    @Override
    public void close() {
        mongoDbManager.close();
        httpClientManager.close();
    }

    public ProcessResult parse(int startPage, int endPage,
                               int nThreadsOfListProcess, int nThreadsOfDetailProcess) {
        log.info("Проверка структуры HTML...");
        boolean changedHtmlStructureInListPage = isChangedHtmlStructureInListPage();
        boolean changedHtmlStructureInDetailPage = isChangedHtmlStructureInDetailPage();
        if (changedHtmlStructureInListPage || changedHtmlStructureInDetailPage) {
            throw new RuntimeException("Структура HTML изменилась");
        }
        log.info("Извлечение данных...");
        return processPages(startPage, endPage, nThreadsOfListProcess, nThreadsOfDetailProcess);
    }

    public String getInfo() {
        String html = getMainPage();
        Document htmlDocument = Jsoup.parse(html);
        int totalNumberOfPages = extractTotalNumberOfPagesInMainPage(htmlDocument);
        int totalNumberOfCatalogs = extractTotalNumberOfCatalogs(htmlDocument);
        return STR."""
        Cайт 'КАТАЛОГ МЕЖГОСУДАРСТВЕННЫХ СТАНДАРТОВ' (https://mgscatalog.by).
        Количество документов:\{totalNumberOfCatalogs}.
        Количество страниц:\{totalNumberOfPages}.
        """;
    }

    public void clearDb() {
        var collection = mongoDbManager.getCollection(Mgscatalog.class);
        collection.deleteMany(new org.bson.Document());
    }

    private ProcessResult processPages(int startPage, int endPage,
                                       int nThreadsOfListProcess, int nThreadsOfDetailProcess) {
        try (ScheduledExecutorService executorService = Executors.newScheduledThreadPool(nThreadsOfListProcess + nThreadsOfDetailProcess)) {
            var collection = mongoDbManager.getCollection(Mgscatalog.class);
            int[][] chunkedPages = Helper.chunkPages(startPage, endPage, nThreadsOfListProcess);
            BlockingQueue<MgscatalogShort> queue = new LinkedBlockingQueue<>();

            for (int i = 0; i < nThreadsOfListProcess; i++) {
                int start = chunkedPages[i][0];
                int end = chunkedPages[i][1];
                executorService.schedule(() -> processListPages(start, end, queue), 1 + i, TimeUnit.SECONDS);
            }

            List<ScheduledFuture<ProcessResult>> tasks = new ArrayList<>();
            for (int i = 0; i < nThreadsOfDetailProcess; i++) {
                tasks.add(executorService.schedule(() -> processDetailPages(collection, queue), 5 + i, TimeUnit.SECONDS));
            }

            int totalSuccess = 0;
            int totalFail = 0;
            StringBuilder totalFailMessage = new StringBuilder();
            for (var task : tasks) {
                ProcessResult result = task.get();
                totalSuccess += result.success();
                totalFail += result.fail();
                totalFailMessage.append(result.failMessage()).append(" ");
            }

            return new ProcessResult(totalSuccess, totalFail, totalFailMessage.toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void processListPages(int startPage, int endPage, BlockingQueue<MgscatalogShort> queue) {
        int i = -1;
        try {
            for (i = startPage; i <= endPage; i++) {
                String html = getListPage(i);
                Set<MgscatalogShort> mgscatalogShortSet = parseListPage(html);
                queue.addAll(mgscatalogShortSet);
                log.info(STR."Страница \{i} из [\{startPage}, \{endPage}] обработана.");
            }
        } catch (Exception e) {
            log.error(STR."Ошибка processListPages, страница \{i}", e);
        }
    }

    private ProcessResult processDetailPages(MongoCollection<org.bson.Document> collection, BlockingQueue<MgscatalogShort> queue) {
        int success = 0, fail = 0;
        StringBuilder sb = new StringBuilder();
        while (!queue.isEmpty()) {
            MgscatalogShort mgscatalogShort = queue.poll();
            if (mgscatalogShort != null) {
                try {
                    String html = getDetailPage(mgscatalogShort);
                    Mgscatalog mgscatalog = parseDetailPage(html);
                    mgscatalog.setId(mgscatalogShort.number());
                    mgscatalog.setDataVvedenia(mgscatalogShort.dataVvedenia());
                    mgscatalog.setDeystvitelenDo(mgscatalogShort.deystvitelenDo());
                    collection.insertOne(mgscatalog.convertToMongoDocument());
                    success++;
                    log.info(STR."Детальная страница \{mgscatalogShort.number()} обработана и сохранена.");
                } catch (Exception e) {
                    log.error(STR."Ошибка processDetailPages, страница \{mgscatalogShort.number()}", e);
                    fail++;
                    sb.append(mgscatalogShort.number()).append(" ");
                }
            }
        }
        return new ProcessResult(success, fail, sb.toString());
    }

    private String getMainPage() {
        try {
            return httpClientManager.executeHttpGetRequest(MAIN_URL);
        } catch (Exception e) {
            throw new RuntimeException("Ошибка getMainPage", e);
        }
    }

    private String getListPage(int pageNumber) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("page", pageNumber);
            return httpClientManager.executeHttpPostRequest(SEARCH_URL, params);
        } catch (Exception e) {
            throw new RuntimeException(STR."Ошибка getListPage, page=\{pageNumber}", e);
        }
    }

    private String getDetailPage(MgscatalogShort mgscatalogShort) {
        try {
            return httpClientManager.executeHttpGetRequest(STR."\{DETAIL_URL}\{mgscatalogShort.number()}");
        } catch (Exception e) {
            throw new RuntimeException(STR."Ошибка getDetailPage, page=\{mgscatalogShort.number()}", e);
        }
    }

    private Mgscatalog parseDetailPage(String html) {
        Document document = Jsoup.parse(html);
        Elements tables = document.select("table.table.table-bordered.table-responsive");
        List<Element> rows = new LinkedList<>(tables.first().select("tbody tr"));

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(OBOZNACHENIYE, null);
        map.put(NAIMENOVANIYE, null);
        map.put(OBLAST_PRIMENENIYA, null);
        map.put(INFORMATSIYA_O_PRINYATII, null);
        map.put(KATEGORIYA, null);
        map.put(SOSTOYANIYE, null);
        map.put(RAZRABOTCHIK, null);
        map.put(ZAKREPLEN_ZA, null);
        map.put(PRISOYEDINIVSHIYESYA_GOSUDARSTVA, null);
        map.put(IZMENENIYA, null);

        for (var row : rows) {
            Elements cols = row.select("td");
            String name = cols.get(0).text();
            if (name.equalsIgnoreCase(IZMENENIYA)) {
                Element table = cols.get(1).selectFirst("table");
                List<MgscatalogChange> mgscatalogChangeList = extractIzmeneniya(table);
                map.put(name, mgscatalogChangeList);
            } else {
                String value = cols.get(1).text();
                map.put(name, value);
            }
        }

        return Mgscatalog.fromMap(map);
    }

    private Set<MgscatalogShort> parseListPage(String html) {
        Set<MgscatalogShort> result = new HashSet<>();
        Document document = Jsoup.parse(html);
        Elements tables = document.select("table.table-bordered.table-responsive");
        Element table = tables.first();
        Elements rows = table.select("tbody tr");

        for (var row : rows) {
            Elements cols = row.select("td");
            String catalogNumber = extractCatalogNumber(cols);
            LocalDateTime dataVvedenia = extractDataVvedenia(cols);
            LocalDateTime deystvitelenDo = extractDeystvitelenDo(cols);

            result.add(new MgscatalogShort(catalogNumber, dataVvedenia, deystvitelenDo));
        }

        return result;
    }

    private int extractTotalNumberOfPagesInMainPage(Document htmlDocument) {
        Elements buttons = htmlDocument.select("button[type=button].btn.btn-primary:containsOwn(последняя)");
        return Integer.parseInt(buttons.first().attr("id"));
    }

    private int extractTotalNumberOfCatalogs(Document htmlDocument) {
        String num = htmlDocument.select("#searchGostResult > div > div > span").text();
        return Integer.parseInt(num);
    }

    private LocalDateTime extractDeystvitelenDo(Elements cols) {
        String text = cols.get(4).text();
        if (text.isBlank()) {
            return null;
        }
        return Helper.parseDateTime(text, Helper.DateTimeFormat.yyyy_MM_dd__HH_mm_ss);
    }

    private LocalDateTime extractDataVvedenia(Elements cols) {
        String text = cols.get(3).text();
        if (text.isBlank()) {
            return null;
        }
        return Helper.parseDateTime(text, Helper.DateTimeFormat.yyyy_MM_dd__HH_mm_ss);

    }

    private String extractCatalogNumber(Elements cols) {
        Element link = cols.get(0).select("a").first();
        String hrefValue = link.attr("href");
        String[] parts = hrefValue.split("=");
        return parts[1];
    }

    private List<MgscatalogChange> extractIzmeneniya(Element table) {
        List<MgscatalogChange> result = new ArrayList<>();
        if (table != null) {
            List<Element> rows = new LinkedList<>(table.select("tbody tr:not(:first-child)"));
            for (var row : rows) {
                Elements cols = row.select("td");
                result.add(MgscatalogChange.builder()
                        .nomerIzmeneniya(cols.get(0).text())
                        .dataVvedeniaIzmeneniya(cols.get(1).text())
                        .kharakter(cols.get(2).text())
                        .istochnik(cols.get(3).text())
                        .build());
            }
        }
        return result;
    }

    private boolean isChangedHtmlStructureInListPage() {
        String html = getMainPage();
        Document htmlDocument = Jsoup.parse(html);

        Elements buttons = htmlDocument.select("button[type=button].btn.btn-primary:containsOwn(последняя)");
        if (buttons.isEmpty() || buttons.first().attr("id").isEmpty()
                || !Helper.isNumeric(buttons.first().attr("id"))
        ) {
            log.warn("Не удалось получить количество страниц.");
            return true;
        }

        String totalNumberOfCatalogs = htmlDocument.select("#searchGostResult > div > div > span").text();
        if (totalNumberOfCatalogs.isBlank() || !Helper.isNumeric(totalNumberOfCatalogs)) {
            log.warn("Не удалось получить количество документов.");
            return true;
        }

        Elements tables = htmlDocument.select("table.table-bordered.table-responsive");
        if (tables.size() != 1) {
            log.warn("Не удалось нати таблицу каталогов.");
            return true;
        }

        Element table = tables.first();
        Element thead = table.selectFirst("thead");
        if (thead == null) {
            log.warn("Не удалось проверить названия столбцов в таблице каталогов.");
            return true;
        }
        Elements tcolumns = thead.select("tr > td");
        if (!OBOZNACHENIYE.equalsIgnoreCase(tcolumns.get(0).text())) {
            log.warn(STR."Столбец '\{OBOZNACHENIYE}' не находится на позиции 1 (или отсутсвует) в таблице каталогов.");
            return true;
        }
        if (!DATA_VVEDENIA.equalsIgnoreCase(tcolumns.get(3).text())) {
            log.warn(STR."Столбец '\{DATA_VVEDENIA}' не находится на позиции 4 (или отсутсвует) в таблице каталогов.");
            return true;
        }
        if (!DEYSTVITELEN_DO.equalsIgnoreCase(tcolumns.get(4).text())) {
            log.warn(STR."Столбец '\{DEYSTVITELEN_DO}' не находится на позиции 5 (или отсутсвует) в таблице каталогов.");
            return true;
        }

        Elements rows = table.select("tbody tr");
        if (rows.isEmpty()) {
            log.warn("Таблица каталогов пустая.");
            return true;
        }

        Elements columns = rows.first().select("td");
        Element link = columns.get(0).select("a").first();
        if (link == null) {
            log.warn(STR."Не удалось найти <a> в столбце '\{OBOZNACHENIYE}'.");
            return true;
        }
        String hrefValue = link.attr("href");
        String[] parts = hrefValue.split("=");
        if (parts.length != 2) {
            log.warn(STR."Не удалось найти значение UrlRN в <a href> '\{link}' в столбце '\{OBOZNACHENIYE}' в таблице каталогов.");
            return true;
        }

        String text = columns.get(3).text();
        if (!text.isBlank()) {
            try {
                Helper.parseDateTime(text, Helper.DateTimeFormat.yyyy_MM_dd__HH_mm_ss);
            } catch (Exception _) {
                log.warn(STR."Не удалось преобразовать значение в столбце '\{DATA_VVEDENIA}' в таблице каталогов.");
                return true;
            }
        }

        return false;
    }

    private boolean isChangedHtmlStructureInDetailPage() {
        String html = getDetailPage(new MgscatalogShort("423493", null, null));
        Document htmlDocument = Jsoup.parse(html);

        Elements tables = htmlDocument.select("table.table.table-bordered.table-responsive");
        if (tables.size() != 1) {
            log.warn("Не удалось нати таблицу детальной информации о каталоге.");
            return true;
        }

        List<Element> rows = new LinkedList<>(tables.first().select("tbody tr"));
        if (rows.isEmpty()) {
            log.warn("Таблица детальной информации о каталоге пустая.");
            return true;
        }

        return false;
    }

}
