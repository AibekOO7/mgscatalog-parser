package kg.cloudsoft.mgscatalogparser.mgscatalog;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.bson.Document;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static kg.cloudsoft.mgscatalogparser.mgscatalog.Constants.*;

@Getter
@Setter
@Builder
public class Mgscatalog {
    private String id;
    private LocalDateTime dataVvedenia;
    private LocalDateTime deystvitelenDo;
    private String oboznacheniye;
    private String naimenovaniye;
    private String oblastPrimeneniya;
    private String informatsiyaOPrinyatii;
    private String kategoriya;
    private String sostoyaniye;
    private String razrabotchik;
    private String zakreplenZa;
    private String prisoyedinivshiyesyaGosudarstva;
    private List<MgscatalogChange> izmeneniya;

    public static Mgscatalog fromMap(Map<String, Object> map) {
        return Mgscatalog.builder()
                .oboznacheniye((String) map.get(OBOZNACHENIYE))
                .naimenovaniye((String) map.get(NAIMENOVANIYE))
                .oblastPrimeneniya((String) map.get(OBLAST_PRIMENENIYA))
                .informatsiyaOPrinyatii((String) map.get(INFORMATSIYA_O_PRINYATII))
                .kategoriya((String) map.get(KATEGORIYA))
                .sostoyaniye((String) map.get(SOSTOYANIYE))
                .razrabotchik((String) map.get(RAZRABOTCHIK))
                .zakreplenZa((String) map.get(ZAKREPLEN_ZA))
                .prisoyedinivshiyesyaGosudarstva((String) map.get(PRISOYEDINIVSHIYESYA_GOSUDARSTVA))
                .izmeneniya((List<MgscatalogChange>) map.get(IZMENENIYA))
                .build();
    }

    public Document convertToMongoDocument() {
        return new Document()
                .append("_id", id)
                .append("dataVvedenia", dataVvedenia)
                .append("deystvitelenDo", deystvitelenDo)
                .append("oboznacheniye", oboznacheniye)
                .append("naimenovaniye", naimenovaniye)
                .append("oblastPrimeneniya", oblastPrimeneniya)
                .append("informatsiyaOPrinyatii", informatsiyaOPrinyatii)
                .append("kategoriya", kategoriya)
                .append("sostoyaniye", sostoyaniye)
                .append("razrabotchik", razrabotchik)
                .append("zakreplenZa", zakreplenZa)
                .append("prisoyedinivshiyesyaGosudarstva", prisoyedinivshiyesyaGosudarstva)
                .append("izmeneniya", izmeneniya == null ? null : izmeneniya.stream().map(MgscatalogChange::convertToMongoDocument).toList());
    }
}
