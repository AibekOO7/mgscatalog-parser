package kg.cloudsoft.mgscatalogparser.mgscatalog;

import kg.cloudsoft.mgscatalogparser.util.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Arrays;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@ShellComponent
public class Commands {

    private final MgscatalogManager mgscatalogManager;

    @ShellMethod(key = "parse", value = "Извлечь и сохранить данне.")
    public String parse(@ShellOption(value = "--startPage", help = "Начальная страница") @Option(required = true) Integer startPage,
                        @ShellOption(value = "--endPage", help = "Последняя страница") @Option(required = true) Integer endPage,
                        @ShellOption(value = "--nThreadsOfListProcess", help = "Кол-во потоков для обработки станиц списка") @Option(required = true) Integer nThreadsOfListProcess,
                        @ShellOption(value = "--nThreadsOfDetailProcess", help = "Кол-во потоков для обработки страниц с подробными сведениями") @Option(required = true) Integer nThreadsOfDetailProcess
    ) {
        ProcessResult result = mgscatalogManager.parse(startPage, endPage, nThreadsOfListProcess, nThreadsOfDetailProcess);
        return STR."""
        Извлечение данных завершено
           Страницы: [\{startPage}, \{endPage}]
           Общее количество документов: \{result.success() + result.fail()}
             Успешно: \{result.success()}
             Неудачно: \{result.fail()}
             \{result.failMessage()}
""";
    }


    @ShellMethod(key = "chunk-pages", value = "Разделить страницы(Пример: [1,10]) на части(Пример: [1,3] [4,6] [7,10]).")
    public String chunkPages(@ShellOption(value = "--start", help = "Начальная страница") @Option(required = true) Integer start,
                             @ShellOption(value = "--end", help = "Последняя страница") @Option(required = true) Integer end,
                             @ShellOption(value = "--parts", help = "На сколько частей нужно разбить") @Option(required = true) Integer parts) {
        StringBuilder stringBuilder = new StringBuilder();
        int[][] chunkedPages = Helper.chunkPages(start, end, parts);
        for (int i = 0; i < chunkedPages.length; i++) {
            stringBuilder.append("Часть ").append(i + 1)
                    .append(": [")
                    .append(
                            Arrays.stream(chunkedPages[i])
                                    .mapToObj(String::valueOf)
                                    .collect(Collectors.joining(", "))
                    )
                    .append("]")
                    .append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }


    @ShellMethod(key = "info", value = "Получить информацию.")
    public String getInfo() {
        return mgscatalogManager.getInfo();
    }

    @ShellMethod(key = "clear-db", value = "Удалить всех записи из бд.")
    public String clearDb() {
        mgscatalogManager.clearDb();
        return "Данные удалены из бд.";
    }

}
