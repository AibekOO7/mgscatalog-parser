package kg.cloudsoft.mgscatalogparser.mgscatalog;

import java.time.LocalDateTime;

public record MgscatalogShort(String number, LocalDateTime dataVvedenia, LocalDateTime deystvitelenDo) {
}
