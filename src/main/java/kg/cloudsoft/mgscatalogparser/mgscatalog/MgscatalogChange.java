package kg.cloudsoft.mgscatalogparser.mgscatalog;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.bson.Document;


@Getter
@Setter
@Builder
public class MgscatalogChange {
    private String nomerIzmeneniya;
    private String dataVvedeniaIzmeneniya;
    private String kharakter;
    private String istochnik;

    public Document convertToMongoDocument() {
        return new Document()
                .append("nomerIzmeneniya", nomerIzmeneniya)
                .append("dataVvedeniaIzmeneniya", dataVvedeniaIzmeneniya)
                .append("kharakter", kharakter)
                .append("istochnik", istochnik);
    }
}
