package kg.cloudsoft.mgscatalogparser.mgscatalog;

public final class Constants {
    final static String MAIN_URL = "https://mgscatalog.by";
    final static String SEARCH_URL = STR."\{MAIN_URL}/script/search.ajax.php";
    final static String DETAIL_URL = STR."\{MAIN_URL}/katalogstand_detail.php?UrlRN=";
    static final String DATA_VVEDENIA = "Дата введения";
    static final String DEYSTVITELEN_DO = "Действителен до";
    static final String OBOZNACHENIYE = "Обозначение";
    static final String NAIMENOVANIYE = "Наименование";
    static final String OBLAST_PRIMENENIYA = "Область применения";
    static final String INFORMATSIYA_O_PRINYATII = "Информация о принятии";
    static final String KATEGORIYA = "Категория";
    static final String SOSTOYANIYE = "Состояние";
    static final String RAZRABOTCHIK = "Разработчик";
    static final String ZAKREPLEN_ZA = "Закреплен за";
    static final String PRISOYEDINIVSHIYESYA_GOSUDARSTVA = "Присоединившиеся государства";
    static final String IZMENENIYA = "Изменения";

    private Constants() {
    }
}
