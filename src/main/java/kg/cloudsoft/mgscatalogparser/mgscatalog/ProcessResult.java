package kg.cloudsoft.mgscatalogparser.mgscatalog;

record ProcessResult(int success, int fail, String failMessage) {
}
