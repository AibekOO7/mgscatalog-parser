package kg.cloudsoft.mgscatalogparser.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public final class MongoDbManager implements AutoCloseable {

    private final MongoClient mongoClient;
    private final MongoDatabase mongoDatabase;

    @Autowired
    public MongoDbManager(MongoDbConfig config) {
        String connectionString = STR."mongodb://\{config.username()}:\{config.password()}@\{config.host()}:\{config.port()}";
        mongoClient = MongoClients.create(connectionString);
        mongoDatabase = mongoClient.getDatabase(config.collection());
    }

    @PostConstruct
    private void postConstruct() {
        try {
            mongoClient.listDatabaseNames().forEach(System.out::println);
            log.info("MongoDb: соединение успешно установлено.");
        } catch (Exception e) {
            throw new RuntimeException("MongoDb: соединение не установлено.", e);
        }
    }

    public MongoCollection<Document> getCollection(Class clazz) {
        return mongoDatabase.getCollection(clazz.getSimpleName());
    }

    @Override
    public void close() {
        mongoClient.close();
    }

    private boolean collectionExists(MongoClient mongoClient, String collectionName) {
        for (String name : mongoClient.listDatabaseNames()) {
            if (name.equals(collectionName)) {
                return true;
            }
        }
        return false;
    }

}
