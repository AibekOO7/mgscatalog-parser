package kg.cloudsoft.mgscatalogparser.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public final class HttpClientManager implements AutoCloseable {
    private final CloseableHttpClient httpClient;
    private final PoolingHttpClientConnectionManager connectionManager;

    @Autowired
    public HttpClientManager(HttpClientConfig config) {
        this.connectionManager = configureConnectionManager(
                config.maxTotalConnections(), config.maxConnectionsPerHost());
        this.httpClient = configureHttpClient(this.connectionManager);
        log.info("HttpClient maxTotalConnections: {}.", config.maxTotalConnections());
        log.info("HttpClient maxConnectionsPerHost: {}.", config.maxConnectionsPerHost());
    }

    public String executeHttpGetRequest(String url) throws IOException {
        HttpGet request = new HttpGet(url);
        return httpClient.execute(request, createResponseHandler());
    }

    public String executeHttpPostRequest(String url, Map<String, Object> params) throws IOException {
        HttpPost request = new HttpPost(url);

        if (!params.isEmpty()) {
            List<BasicNameValuePair> formParams = new ArrayList<>();
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                formParams.add(new BasicNameValuePair(
                        entry.getKey(),
                        String.valueOf(entry.getValue())
                ));
            }
            request.setEntity(new UrlEncodedFormEntity(formParams, StandardCharsets.UTF_8));
        }

        return httpClient.execute(request, createResponseHandler());
    }

    @Override
    public void close() {
        try {
            httpClient.close();
        } catch (Exception e) {
            log.error("Ошибка при закрытии HttpClient.", e);
        } finally {
            connectionManager.close();
        }
    }

    private PoolingHttpClientConnectionManager configureConnectionManager(int maxTotalConnections,
                                                                          int maxConnectionsPerHost) {
        PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
        manager.setMaxTotal(maxTotalConnections);
        manager.setDefaultMaxPerRoute(maxConnectionsPerHost);
        return manager;
    }

    private CloseableHttpClient configureHttpClient(PoolingHttpClientConnectionManager connectionManager) {
        return HttpClients.custom()
                .setConnectionManager(connectionManager)
                .build();
    }

    private HttpClientResponseHandler<String> createResponseHandler() {
        return response -> EntityUtils.toString(response.getEntity());
    }

}
