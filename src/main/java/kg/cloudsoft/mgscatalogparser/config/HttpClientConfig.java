package kg.cloudsoft.mgscatalogparser.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "http-client")
public record HttpClientConfig(
        int maxTotalConnections,
        int maxConnectionsPerHost) {
}
