package kg.cloudsoft.mgscatalogparser.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mongodb")
public record MongoDbConfig(
        String host,
        int port,
        String collection,
        String username,
        String password) {
}

