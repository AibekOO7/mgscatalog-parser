plugins {
	java
	id("org.springframework.boot") version "3.2.1"
	id("io.spring.dependency-management") version "1.1.4"
}

group = "kg.cloudsoft"
version = "1.0.0"

java {
	sourceCompatibility = JavaVersion.VERSION_21
}

repositories {
	mavenCentral()
	maven { url = uri("https://repo.spring.io/milestone") }
}

extra["springShellVersion"] = "3.2.0-RC1"

dependencies {
	implementation("org.springframework.shell:spring-shell-starter")
	implementation("org.jsoup:jsoup:1.17.1")
	implementation("org.mongodb:mongodb-driver-sync:4.11.1")
	implementation("org.apache.httpcomponents.client5:httpclient5:5.3")

	compileOnly("org.projectlombok:lombok:1.18.30")
	annotationProcessor("org.projectlombok:lombok:1.18.30")
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.shell:spring-shell-dependencies:${property("springShellVersion")}")
	}
}

springBoot {
	buildInfo()
}

tasks.withType<JavaCompile> {
	options.compilerArgs.add("--enable-preview")
}

tasks.withType<JavaExec> {
	jvmArgs("--enable-preview")
}

tasks.withType<Test> {
	useJUnitPlatform()
	jvmArgs("--enable-preview")
}
